package mapexample;

import mapexample.data.GeoLocation;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

@RestController
public class MapController {

    private static final String apiKey = "<api-key>";
    private static final String  localUrl = "https://www.googleapis.com/geolocation/v1/geolocate";

    @RequestMapping("/")
    public String index() {
        String geoUrl = localUrl + "?key=" + apiKey;
        RestTemplate restTemplate = new RestTemplate();
        GeoLocation geoLocation = restTemplate.postForObject(geoUrl,null, GeoLocation.class);
        return geoLocation.toString();
    }

}