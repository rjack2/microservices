package mapexample.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by rjackson on 1/6/2017.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GeoLocation {
    Location location;
    String accuracy;

    public GeoLocation() {
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    @Override
    public String toString() {
        return "GeoLocation{" +
                "location=" + location +
                ", accuracy='" + accuracy + '\'' +
                '}';
    }
}
